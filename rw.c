#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char *argv[])
{
    uint a = 18;
    rwinit();
    // rwtest(a);
    int find_one = 0;
    for (int i = 7; i >= 0; i--)
        if (a & (1 << i))
        {
            if(find_one == 0){
                find_one = 1;
                continue;
            }
            int pid = fork();
            if (pid == 0)
            {
                write_test();
                exit();
            }
        }
        else if(find_one)
        {
            int pid = fork();
            if (pid == 0)
            {
                read_test();
                exit();
            }
        }

    int t = wait();
    while (t >= 0)
    {
        t = wait();
    }

    exit();
}